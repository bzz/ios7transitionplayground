//
//  Transition.h
//  UIKitTransitionsTest1
//
//  Created by Mikhail Baynov on 30/07/14.
//  Copyright (c) 2014 Brisk Mobile Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Transition : NSObject
<UIViewControllerTransitioningDelegate, UIViewControllerAnimatedTransitioning>


@end
