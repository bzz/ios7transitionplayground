//
//  ViewController.m
//  VCTransitionPlayground
//
//  Created by Mikhail Baynov on 31/07/14.
//  Copyright (c) 2014 StarDreams. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    SecondViewController *vc2 = [SecondViewController new];
    Transition *transition = [Transition new];
//    self.transitioningDelegate = transition;
    vc2.transitioningDelegate = transition;
    [self presentViewController:vc2 animated:YES completion:^{        
    }];
}

@end
