//
//  Transition.m
//  UIKitTransitionsTest1
//
//  Created by Mikhail Baynov on 30/07/14.
//  Copyright (c) 2014 Brisk Mobile Inc. All rights reserved.
//

#import "Transition.h"

@implementation Transition
//===================================================================
#pragma mark - UIViewControllerTransitioningDelegate
//===================================================================

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
    return self;
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    return nil;
}

- (id <UIViewControllerInteractiveTransitioning>)interactionControllerForPresentation:(id <UIViewControllerAnimatedTransitioning>)animator {
    return nil;
}

- (id <UIViewControllerInteractiveTransitioning>)interactionControllerForDismissal:(id <UIViewControllerAnimatedTransitioning>)animator {
    return nil;
}


//===================================================================
#pragma mark - UIViewControllerAnimatedTransitioning
//===================================================================

- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext {
    return 40;
}

- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext {
    
    UIViewController *fromVC = (UIViewController *)[transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey]; // Should be quals to self
    UIViewController *toVC = (UIViewController *)[transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    
    [[transitionContext containerView] addSubview:toVC.view];
    CGRect screenRect = [[UIScreen mainScreen] bounds];
//    fromVC.view.center = CGPointMake(screenRect.size.width *1/2, screenRect.size.height *1/2);
    toVC.view.center = CGPointMake(screenRect.size.width *-1/2, screenRect.size.height *1/2);
    
    [UIView animateWithDuration:.2 animations:
     ^{
//         fromVC.view.center = CGPointMake(screenRect.size.width *1/2, screenRect.size.height *-1/2);

         CATransform3D  rot = CATransform3DMakeScale(.9, .9, 1);
         rot.m34=-1.0 / 200;
         [fromVC.view.layer setTransform:rot];

         toVC.view.center = CGPointMake(screenRect.size.width/2, screenRect.size.height/2);
     } completion:^(BOOL finished) {
         [transitionContext completeTransition:YES];
     }];
}



@end
