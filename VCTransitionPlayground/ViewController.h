//
//  ViewController.h
//  VCTransitionPlayground
//
//  Created by Mikhail Baynov on 31/07/14.
//  Copyright (c) 2014 StarDreams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Transition.h"
#import "SecondViewController.h"

@interface ViewController : UIViewController

@end
